package com.example.lordxyroz.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class A1 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a1);

        final Spinner spinner = findViewById(R.id.a1Spinner);
        final Button btn = findViewById(R.id.a1Button);

        List<String> categories = new ArrayList<>();
        categories.add("Test1");
        categories.add("Test2");
        categories.add("temp123");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final int L1 = prefs.getInt("L1", -1);
        if (L1 != -1) {
            spinner.setSelection(L1);
        }

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(A1.this, A2.class));
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = prefs.edit();

        final Spinner spinner = findViewById(R.id.a1Spinner);
        final EditText text = findViewById(R.id.a1EditText);

        final int L1 = spinner.getSelectedItemPosition();
        editor.putInt("L1", L1);

        final String T1 = text.getText().toString();
        editor.putString("T1", T1);

        editor.apply();
    }
}
