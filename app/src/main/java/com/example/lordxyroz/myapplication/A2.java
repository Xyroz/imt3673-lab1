package com.example.lordxyroz.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class A2 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a2);

        final TextView text = findViewById(R.id.a2TextView);
        final TextView text2 = findViewById(R.id.a2TextView2);
        final Button btn = findViewById(R.id.a2Button);

        text.setText("Hello ");
        text2.setText("From A3: ");

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final String T1 = prefs.getString("T1", "");

        if (T1 != "") {
            text.setText(text.getText() + T1);
        }

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(A2.this, A3.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        final TextView text2 = findViewById(R.id.a2TextView2);
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        final String T4 = prefs.getString("T4", "");

        if (T4 != "") {
            text2.setText(text2.getText() + T4);
        }
    }
}
