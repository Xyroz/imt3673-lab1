package com.example.lordxyroz.myapplication;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.EditText;
import android.widget.Button;
import android.view.View;

public class A3 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a3);

        final Button btn = findViewById(R.id.a3Button);
        final EditText text = findViewById(R.id.a3EditText);

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = prefs.edit();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String T4 = text.getText().toString();

                editor.putString("T4", T4);
                editor.apply();
                finish();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        final EditText text = findViewById(R.id.a3EditText);
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = prefs.edit();

        final String T4 = text.getText().toString();

        editor.putString("T4", T4);
        editor.apply();
    }
}